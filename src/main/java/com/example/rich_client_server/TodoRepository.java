package com.example.rich_client_server;


import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Umhüllt das eigentliche Repository, um eine explizite Schnittstelle zu bilden. Es
 * muss ein {@link JpaRepository<Todo, Long>} mit dem Namen SpringTodoRepository
 * erstellt und hier entsprechend als Delegate verwendet werden.
 */
public class TodoRepository {
}
